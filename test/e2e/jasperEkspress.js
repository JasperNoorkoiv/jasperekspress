const config = require('../../nightwatch.conf.js');

module.exports = {
  '@tags': ['EestiEkspress1'],
  'Eesti Ekspress 1': function (browser) {
    browser.resizeWindow(1920, 1080)
      .url('http://ekspress.delfi.ee')
      .waitForElementVisible('body')
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}esimenepilt.png`)
      .click('a[class="headline__media"]')
      .pause(1000)
    // .moveToElement('a[class="navigation-arrow navigation-arrow-right"]',1,1)
    // .click('div[class="next-article-preview"]')
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}ekspress.png`)



      .waitForElementVisible('body')
      .click('a[class="header__menu-burger toggle-sidebar"]')
      .waitForElementVisible('body')
      .pause(1000)
      .click('a[class="channels__link"]')
      .pause(1000)
      .end();
  },
};